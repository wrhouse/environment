function error_message(code, reason)
  return cjson.encode({
    success = false, 
    error = {
      code = code, 
      message = reason
    }
  })
end

local cjson = require("cjson")
local access = ngx.var.arg_access_token or ""
local refresh = ngx.var.arg_refresh_token or ""
local jwt_tokens = ngx.var.arg_tokens or ""
jwt_pattern = "^[A-Za-z0-9-_=]+%.[A-Za-z0-9-_=]+%.?[A-Za-z0-9-_.+/=]*$"
if not access:match(jwt_pattern) then
  ngx.say(error_message(1404, "Bad access token. (" .. access .. ")"))
  ngx.exit(ngx.OK)
end
if not refresh:match(jwt_pattern) then
  ngx.say(error_message(1404, "Bad refresh token.(".. refresh .. ")"))
  ngx.exit(ngx.OK)
end
local host = ngx.var.http_host
local service_id = host:gsub("(%w+)%.(.*)", "%1")
-- shop has no subdomains. 
-- So shop service_id will be like "gaming-warehousdev" or "gaming-warehouse" on prod.
if service_id:match("gaming") then
  service_id = "shop"
end
local expires = ""
ngx.header['Set-Cookie'] = {
  "refresh_token=" .. refresh .. "; path=/;",
  "access_token=" .. access .. "; path=/;"
}
return ngx.redirect("https://profile.gaming-warehousedev.com/api/v1/session/redirects/login?service_id=" .. service_id .. "&tokens=" .. jwt_tokens)
