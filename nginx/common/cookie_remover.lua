local ref_tok = ngx.var.cookie_refresh_token or ""
local acc_tok = ngx.var.cookie_access_token or ""

ngx.header['Set-Cookie'] = {
  "refresh_token=" .. ref_tok .. [[; path=/;Expires='Wed, 31 Oct 1971 08:50:17 GMT']],
  "access_token=" .. acc_tok .. [[; path=/;Expires='Wed, 31 Oct 1971 08:50:17 GMT']]
}
local services = ngx.var.arg_services
local host = ngx.var.http_host
local service_id = host:gsub("(%w+)%.(.*)", "%1")
-- shop has no subdomains. 
-- So shop service_id will be like "gaming-warehousdev" or "gaming-warehouse" on prod.
if service_id:match("gaming") then
  service_id = "shop"
end
return ngx.redirect("https://profile.gaming-warehousedev.com/api/v1/session/redirects/logout?service_id=" .. service_id .. "&services=" .. services)
