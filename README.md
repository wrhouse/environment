<div align="center">
<h1>
  <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Nginx_logo.svg/220px-Nginx_logo.svg.png" alt="Logo" width="200">
</h1>

<h3 align="center">Local Nginx servcie</h3>

<b>Service that creates enviroment to run full gaming-warehouse ecosystem locally.</b>
</div>

# Usage

Before startup change your hosts file.

Hosts file can be found in:
- `/etc/hosts` for unix-systems 
- `C:\windows\system32\drivers\etc\hosts` for windows. 

Add folowing line in your hosts:
```
0.0.0.0         gaming-warehousedev.com profile.gaming-warehousedev.com forum.gaming-warehousedev.com
```

After editing check your internet connection and run nginx service with following command.

```bash
docker-compose up --build
```
