FROM nginx:1.17.2
RUN uname -a
RUN \
  apt-get update &&\
  apt-get install -y nginx-extras lua-cjson &&\
  rm -rf /var/lib/apt/lists/*
